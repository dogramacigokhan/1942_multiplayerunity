# README #

### What is this repository for? ###

This is a online multiplayer Unity3D implementation of the classic 1942 game. All paid plugins are removed, but you may still learn lots of things from the source code.

Sample Screenshot:
![ss.png](https://bitbucket.org/repo/x6LqMR/images/740137372-ss.png)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact