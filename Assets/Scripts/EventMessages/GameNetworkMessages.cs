﻿public enum GameNetworkMessages
{
    CreatedRoom,
    JoinedRoom,

    DisconnectFromServer
}

public class OnCreateRoom
{
    public string RoomName;
}

public class OnJoinRoom
{
    public string RoomName;
}

public class OnCancelCreateOrJoinRoom
{
    public string RoomName;
}