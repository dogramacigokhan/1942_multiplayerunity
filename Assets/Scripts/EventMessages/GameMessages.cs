﻿public enum GameMessages
{
    GameStarted,
    GamePaused,
    GameResumed,
    GameOverLocally,
    GameOverGlobally,
    Exit,

    WaveCreationStarted,
    WaveCreationStopped,

    EnemyShipIsLost
}

public class PlayerShipRevivedMessage
{
    public int RemainingLives;
}

public class EnemyShipDestroyedMessage
{
    public EnemyShip EnemyShip;
}

public class LevelCompletedMessage
{
    public int CompletedLevel;
}

public class ScoreUpdatedMessage
{
    public int NewScore;
}

public class HighScoreUpdatedMessage
{
    public int NewHighScore;
}