﻿using UnityEngine;

public abstract class Ship : Photon.MonoBehaviour
{
    public int HitPoints { get; protected set; }

    public abstract void ReceiveDamage(int amount);

    protected abstract void Destroy(int playerType = 0, PhotonMessageInfo info = null);
}
