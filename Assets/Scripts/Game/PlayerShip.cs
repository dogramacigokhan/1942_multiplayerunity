﻿using System.Collections;
using System.Linq;
using PathologicalGames;
using UnityEngine;

public enum PlayerType
{
    PlayerOne,
    PlayerTwo
}  

/// <summary>
/// Represents player's ship on singleplayer
/// Represents both Player 1 and Player 2 ships on multiplayer.
/// </summary>
public class PlayerShip : Ship, IHandle<GameMessages>, IHandle<GameNetworkMessages>
{

    public PlayerType PlayerType;               // Editor

    public int InitialLives;                    // Editor
    public int InitialHitPoints;                // Editor

    public float BlinkDurationInSeconds;        // Editor

    public Transform ExplosionPrefab;           // Editor
    public UISprite UpgradePrefab;              // Editor

    public bool CanReceiveDamage { get; private set; }

    public PlayerShipController ShipController;
    public PlayerShipShooter ShipShooter;

    private int _lives;                         

    private Vector3 _initialPosition;

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);

        ShipController.enabled = false;
        ShipShooter.enabled = false;
        gameObject.SetActive(false);

        _initialPosition = transform.position;
    }

    public void Handle(GameMessages message)
    {
        switch (message)
        {
            case GameMessages.GameStarted:
                Init();
                break;
            case GameMessages.GameOverLocally:
                if (PhotonNetwork.offlineMode)
                    gameObject.SetActive(false);
                break;
            case GameMessages.GameOverGlobally:
                gameObject.SetActive(false);
                break;
        }
    }

    public void Handle(GameNetworkMessages message)
    {
        switch (message)
        {
            case GameNetworkMessages.DisconnectFromServer:
                OtherPlayerDisconnected();
                break;
        }
    }

    private void Init()
    {
        gameObject.SetActive(true);
        ShipShooter.enabled = ShipController.enabled = true;

        if (PhotonNetwork.offlineMode)
        {
            // Singleplayer mode
            if (PlayerType == PlayerType.PlayerTwo)
            {
                // Disable second player's ship
                gameObject.SetActive(false);
                return;
            }
        }
        else
        {
            // Multiplayer mode
            if (PhotonNetwork.isMasterClient && PlayerType == PlayerType.PlayerTwo)
            {
                // If we are first player and this is the ship for second  player, let second player own this
                photonView.TransferOwnership(PhotonNetwork.otherPlayers[0]);
                ShipShooter.enabled = false;
            }
            else if (!PhotonNetwork.isMasterClient && PlayerType == PlayerType.PlayerOne)
            {
                // If we are second player and this is the ship for first player, just disable it's controllers
                ShipShooter.enabled = false;
            }
        }

        _lives = InitialLives;
        Revive();
    }

    [RPC]
    private void Revive()
    {
        CanReceiveDamage = false;

        HitPoints = InitialHitPoints;
        ShipShooter.Reset();

        transform.position = InitPosition();
        gameObject.SetActive(true);

        EventAggregator.Instance.Publish(new PlayerShipRevivedMessage
        {
            RemainingLives = _lives
        });
        StartCoroutine(BlinkCoro());
    }

    private Vector3 InitPosition()
    {
        if (PhotonNetwork.offlineMode)
            return _initialPosition; // On singleplayer mode, player always will be on the center

        // First player will be on left, and second player will be on right
        var delta = new Vector3(0.5f, 0) * (PhotonNetwork.isMasterClient ? -1 : 1);
        return _initialPosition + delta;
    }

    // Our ship blinks for a while when it is revived and 
    // it can't receive damages in this duration.
    private IEnumerator BlinkCoro()
    {
        var startTime = Time.timeSinceLevelLoad;
        var sprite = GetComponent<UISprite>();

        while (Time.timeSinceLevelLoad - startTime < BlinkDurationInSeconds)
        {
            sprite.enabled = !sprite.enabled;
            yield return new WaitForSeconds(0.25f);
        }

        sprite.enabled = true;
        CanReceiveDamage = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var otherSprite = other.GetComponent<UISprite>();
        if (otherSprite != null && otherSprite.spriteName == UpgradePrefab.spriteName)
        {
            // Shooting upgrade
            if (PhotonNetwork.offlineMode || PhotonNetwork.isMasterClient)
                ShipShooter.Upgrade(null);
            PoolManager.Pools["Pool"].Despawn(other.transform);
        }

        if (!PhotonNetwork.offlineMode && !PhotonNetwork.isMasterClient)
            return; // Only the master client can trigger other collision events

        if (!CanReceiveDamage)
            return;

        var enemyShip = other.GetComponent<EnemyShip>();
        if (enemyShip != null)
        {
            // Hit to enemy ship
            if (PhotonNetwork.offlineMode)
            {
                enemyShip.ReceiveDamage(enemyShip.HitPoints);
                Destroy();
            }
            else
            {
                enemyShip.ReceiveDamage(enemyShip.HitPoints);
                photonView.RPC("Destroy", PhotonTargets.AllViaServer, (int)PlayerType);
            }
        }

        // Bullets have their own trigger method so we don't
        // need to check any bullet trigger here.
    }

    [RPC]
    public override void ReceiveDamage(int amount)
    {

        Debug.Log("Receive damage: " + transform.parent.name + " is mine: " + photonView.isMine);
        HitPoints -= amount;
        if (HitPoints > 0) return;

        if (PhotonNetwork.offlineMode)
            Destroy();
        else
            photonView.RPC("Destroy", PhotonTargets.All, (int)PlayerType);
    }

    [RPC]
    protected override void Destroy(int playerType = 0, PhotonMessageInfo info = null)
    {
        if (--_lives <= 0)
        {
            // Destroy animation before despawning ship
            DestroyAnimation();
            ShipController.enabled = false;

            if (PhotonNetwork.offlineMode)
            {
                // We have no lives left. Game is over for us.
                EventAggregator.Instance.Publish(GameMessages.GameOverLocally);
                gameObject.SetActive(false);
            }
            else
            {
                if (PlayerType == (PlayerType) playerType)
                {
                    // We have no lives left. Game is over for us.
                    EventAggregator.Instance.Publish(GameMessages.GameOverLocally);
                    gameObject.SetActive(false);

                    if (PhotonNetwork.isMasterClient && FindObjectOfType<PlayerShip>() == null)
                        photonView.RPC("GameOverGlobally", PhotonTargets.All);
                }
            }
        }
        else
        {
            // Destroy animation before despawning ship
            DestroyAnimation();
            Revive();
        }
    }

    [RPC]
    private void GameOverGlobally(PhotonMessageInfo info)
    {
        EventAggregator.Instance.Publish(GameMessages.GameOverGlobally);
    }

    private void OtherPlayerDisconnected()
    {
        // Disable disconnected players ship
        var ships = FindObjectsOfType<PlayerShip>();
        if (ships.Length > 1)
            ships.First(s => s.PlayerType != PlayerType).gameObject.SetActive(false);
    }

    private void DestroyAnimation()
    {
        var explosion = PoolManager.Pools["Pool"].Spawn(ExplosionPrefab, transform.parent);
        explosion.localScale = Vector3.one;
        explosion.transform.position = transform.position;
    }
}
