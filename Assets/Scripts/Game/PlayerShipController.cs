﻿using UnityEngine;

public class PlayerShipController : Photon.MonoBehaviour, IHandle<GameMessages>
{

    public float Speed = 1;         // Editor

    private Vector2 _minXY;         // Minimum XY coordinates of the screen in world space
    private Vector2 _maxXY;         // Maximum XY coordinates of the screen in world space

    private float _lastSynchronizationTime;
    private float _syncDelay;
    private float _syncTime;
    private Vector3 _syncStartPosition = Vector3.zero;
    private Vector3 _syncEndPosition = Vector3.zero;

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
        var sprite = GetComponent<UISprite>();

        _minXY = Camera.main.ScreenToWorldPoint(
            new Vector3((float) sprite.width/2, (float) sprite.height/2));
        _maxXY = Camera.main.ScreenToWorldPoint(
            new Vector3(Screen.width - (float)sprite.width / 2, Screen.height - (float)sprite.height / 2));
    }

    void Update()
    {
        if (PhotonNetwork.offlineMode || photonView.isMine)
            InputMovement();    // Movement of our player in our view
        else
            SyncedMovement();   // Movement of second player in our view
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // Sending position data to server
            stream.SendNext(transform.position);
        }
        else
        {
            // Receiving position data
            _syncEndPosition = (Vector3)stream.ReceiveNext();
            _syncStartPosition = transform.position;

            _syncTime = 0f;
            _syncDelay = Time.time - _lastSynchronizationTime;
            _lastSynchronizationTime = Time.time;
        }
    }

    private void InputMovement()
    {
        var deltaPostion = new Vector3(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        var targetPosition = transform.position + deltaPostion;

        transform.position = Vector3.Lerp(transform.position, targetPosition,
            Time.deltaTime*Speed/deltaPostion.magnitude);

        // Keep ship inside the screen coordinates
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, _minXY.x, _maxXY.x),
            Mathf.Clamp(transform.position.y, _minXY.y, _maxXY.y)
        );
    }

    private void SyncedMovement()
    {
        // Interpolating position to get rid of laggy displacement
        _syncTime += Time.deltaTime;
        transform.position = Vector3.Lerp(_syncStartPosition, _syncEndPosition, _syncTime / _syncDelay);
    }

    public void Handle(GameMessages message)
    {
        switch (message)
        {
            case GameMessages.GamePaused:
                enabled = false;
                break;
            case GameMessages.GameResumed:
                enabled = true;
                break;
        }
    }
}
