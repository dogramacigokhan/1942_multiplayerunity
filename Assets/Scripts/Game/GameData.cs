﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class GameData
{
    private const string DataFile = "gameData.dat";

    public static void SetData(Data gameData)
    {
        // We are saving the game data as a binary file so
        // it'll be very hard to modify (hack) it without knowing the structure.
        var bf = new BinaryFormatter();
        var file = File.OpenWrite(Application.persistentDataPath + Path.AltDirectorySeparatorChar + DataFile);

        bf.Serialize(file, gameData);
        file.Close();
    }

    public static Data GetData()
    {
        // Read the game data if it exists on the file system.
        var filePath = Application.persistentDataPath + Path.AltDirectorySeparatorChar + DataFile;
        if (!File.Exists(filePath))
            throw new DirectoryNotFoundException("Can not find game data.");

        var bf = new BinaryFormatter();
        var file = File.OpenRead(filePath);

        var data = (Data) bf.Deserialize(file);
        file.Close();

        return data;
    } 
}

[System.Serializable]
public class Data
{
    // Our serializable game data which only contains high scores for now.
    public List<int> HighScores;
}
