﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using PathologicalGames;
using MonoBehaviour = Photon.MonoBehaviour;

public static class BulletData
{
    // There is no need to re-calculate movement distance for each bullet.
    // We can set the destination as the hypotenuse of screen dimensions
    // so, the bullets will be always visible while in the screen.
    public static readonly float Distance =
        Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height)).magnitude * 2;

    public const float Speed = 1.5f;
}

public class BulletManager : MonoBehaviour
{

    public GameObject BulletPrefab;     // Editor

    private const float BulletDistance = 15f;

    [RPC]
    public void Fire(Vector3 fromPosition, Vector3 toPosition, int bulletOwnerType)
    {
        // Spawn and init bullet
        var bulletTransform = PoolManager.Pools["Pool"].Spawn(BulletPrefab, transform);
        bulletTransform.localScale = Vector3.one;
        bulletTransform.transform.position = fromPosition;

        var bullet = bulletTransform.GetComponent<Bullet>();
        var direction = toPosition - fromPosition;

        Fire(bullet, direction, (BulletOwnerType)bulletOwnerType);
    }

    public void Fire(Bullet bullet, Vector3 direction, BulletOwnerType owner)
    {
        bullet.Owner = owner;

        // Fire the bullet on the desired direction
        bullet.transform.DOMove(bullet.transform.position + direction * BulletData.Distance, BulletData.Speed)
            .SetSpeedBased(true)
            .SetEase(Ease.Linear)
            .OnComplete(bullet.Despawn);
    }

    [RPC]
    public void FireMultiple(Vector3 fromPosition, int level)
    {
        for (var i = 0; i < level; i++)
        {
            // Spawn bullets
            var bulletTransform = PoolManager.Pools["Pool"].Spawn(BulletPrefab, transform.parent);
            bulletTransform.localScale = Vector3.one;

            // Multiple bullets should be fired symmetrically. So a little adjustment is needed here.
            bulletTransform.position = fromPosition +
                transform.TransformPoint(new Vector3(i*BulletDistance - (level - 1)*(BulletDistance/2), 0));

            // We have initialized bullets, so we can now fire them
            Fire(bulletTransform.GetComponent<Bullet>(), Vector3.up, BulletOwnerType.Player);
        }
    }
}
