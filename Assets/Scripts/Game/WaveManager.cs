﻿using System;
using System.Collections.Generic;
using PathologicalGames;
using UnityEngine;
using System.Collections;
using System.Linq;
using Random = UnityEngine.Random;

public class WaveManager : Photon.MonoBehaviour, IHandle<GameMessages>, IHandle<LevelCompletedMessage>
{

    public Transform EnemyShipsParent;          // Editor

    public float FirstWaveDelayInSeconds;       // Editor
    public float WaveDurationInSeconds;         // Editor
    
    public float MinDelayBetweenWavesInSecond;  // Editor
    public float MaxDelayBetweenWavesInSecond;  // Editor

    public float MinMultipleShipCount;          // Editor
    public float MinSpeed;                      // Editor


    // Movement list with their probabilities
    private static readonly Dictionary<Type, float> Movements = new Dictionary<Type, float>
    {
        {typeof (DirectionalAI), 1f}, 
        {typeof (DirectionalReversedAI), 0.05f}, 
        {typeof (SideTopBottomAI), 0.8f}, 
        {typeof (SideBottomTopAI), 0.5f}
    };

    private float _waveStartTime;

    private int _waveCount;
    private float _waveSpeed;

    private int _currentLevel;

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    public void Handle(GameMessages message)
    {
        switch (message)
        {
            case GameMessages.GameStarted:
                _currentLevel = 1;
                StartCoroutine("StartCreatingWaves");
                break;
            case GameMessages.GameOverLocally:
                if (PhotonNetwork.offlineMode)
                    StopAllCoroutines();
                break;
            case GameMessages.GameOverGlobally:
            case GameMessages.Exit:
                StopAllCoroutines();
                break;
        }
    }

    public void Handle(LevelCompletedMessage message)
    {
        StopAllCoroutines();
        _currentLevel = message.CompletedLevel + 1;
        StartCoroutine("StartCreatingWaves");
    }

    public IEnumerator StartCreatingWaves()
    {
        if (!PhotonNetwork.offlineMode && !PhotonNetwork.isMasterClient)
            yield break;    // Only master client should create waves

        yield return new WaitForSeconds(FirstWaveDelayInSeconds);

        // Wave delay starts from maximum duration and it decreases
        // until the minimum value with an inversed logarithmic curve.
        var waveDelay = (MaxDelayBetweenWavesInSecond - MinDelayBetweenWavesInSecond)/_currentLevel +
                        MinDelayBetweenWavesInSecond;

        // Wave count starts from the minimum count and it increases
        // forever with ln(x) curve.
        _waveCount = (int)(Mathf.Log(_currentLevel) + MinMultipleShipCount);

        // Wave speed starts from the minimum value and it increases
        // forever with logarithmic curve.
        _waveSpeed = Mathf.Log10(_currentLevel) + MinSpeed;

        EventAggregator.Instance.Publish(GameMessages.WaveCreationStarted);

        // Start creating waves
        _waveStartTime = Time.timeSinceLevelLoad;
        while (Time.timeSinceLevelLoad - _waveStartTime < WaveDurationInSeconds)
        {
            // Select random ship type for the wave
            var shipType = (int)Enum.GetValues(typeof(EnemyShipType)).GetValue(Random.Range(0, 3));

            // Get a random movement script to the first ship of the wave
            var movementId = GetRandomMovementAi();

            // Get a random start position by using this game object as target
            var startPos = AddMovementAi(gameObject, Movements.Keys.ToList()[movementId]).StartMovement();

            if (PhotonNetwork.offlineMode)
                StartCoroutine("SendMultipleShipsCoro", new List<object>() {shipType, movementId, _waveCount, _waveSpeed, startPos});
            else
                photonView.RPC("SendMultipleShips", PhotonTargets.AllViaServer, shipType, movementId, _waveCount, _waveSpeed, startPos);

            // Add some randomness to the wave delay
            yield return new WaitForSeconds((waveDelay + Random.Range(-1f, 1f)));
        }

        // Duration completed, no waves will be send until the next level
        EventAggregator.Instance.Publish(GameMessages.WaveCreationStopped);
    }

    [RPC]
    private void SendMultipleShips(int shipType, int movementId, int waveCount, float waveSpeed, Vector3 startPos, PhotonMessageInfo info)
    {
        StartCoroutine("SendMultipleShipsCoro", new List<object>() {shipType, movementId, waveCount, waveSpeed, startPos});
    }

    private IEnumerator SendMultipleShipsCoro(IList<object> parameters)
    {
        var shipType = (int)parameters[0];
        var movementId = (int)parameters[1];
        var waveCount = (int)parameters[2];
        var waveSpeed = (float)parameters[3];
        var startPosition = (Vector3)parameters[4];

        for (var i = 0; i < waveCount; i++)
        {
            // Get a ship from the pool
            var ship = SpawnEnemyShip((EnemyShipType)shipType);
            ship.Init((EnemyShipType)shipType);

            var movementAi = AddMovementAi(ship.gameObject, Movements.Keys.ToList()[movementId]);
            movementAi.StartMovement(startPosition, waveSpeed);

            yield return new WaitForSeconds(1 / (movementAi.Speed * 4));
        }
    }

    private int GetRandomMovementAi()
    {
        var randomResult = Random.Range(0f, 1f);

        // We don't want the same probabilities for each level.
        // So we need to equalize all probabilities at some point.
        // We use logarithmic curve for that. So in high levels,
        // all the probabilities will be almost 1.
        var adjustment = Mathf.Clamp01(Mathf.Log10(_currentLevel));

        var movements = new List<Type>();
        foreach (var probability in Movements)
        {
            // Add all possible movement scripts
            if (randomResult < Mathf.Abs(probability.Value + (1 - probability.Value)*adjustment))
                movements.Add(probability.Key);
        }

        // Select one of the movement scripts inside all of the possible scripts
        return movements.Count > 0 ? Random.Range(0, movements.Count) : 0;
    }

    private MovementAI AddMovementAi(GameObject obj, Type type)
    {
        if (type == typeof(DirectionalAI))
            return obj.AddComponent<DirectionalAI>();
        if (type == typeof(DirectionalReversedAI))
            return obj.AddComponent<DirectionalReversedAI>();
        if (type == typeof(SideTopBottomAI))
            return obj.AddComponent<SideTopBottomAI>();
        if (type == typeof(SideBottomTopAI))
            return obj.AddComponent<SideBottomTopAI>();
        return null;
    }

    private EnemyShip SpawnEnemyShip(EnemyShipType shipType)
    {
        var ship = PoolManager.Pools["Pool"].Spawn(shipType.ToString(), EnemyShipsParent);
        ship.transform.localScale = Vector3.one;
        return ship.GetComponent<EnemyShip>();
    }
}
