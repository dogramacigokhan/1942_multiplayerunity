﻿using DG.Tweening;
using UnityEngine;

public class SideBottomTopAI : MovementAI
{

    protected float XToSpawnFromLeft;

    protected float XToSpawnFromRight;

    void OnDisable()
    {
        transform.DOKill();
    }

    public override void Init()
    {
        base.Init(); 

        // This kind of ships will be spawned from the sides of the screen with an angle.
        // So we don't want them to be seen as soon as they are spawned, therefore 
        // we need to move them by the hypotenuse of their sprites.
        var hypotenuse = new Vector2(sWidth, sHeight).magnitude;
        XToSpawnFromLeft = -hypotenuse;
        XToSpawnFromRight = Screen.width + hypotenuse;
    }

    public override Vector3 StartMovement(float speed = 1)
    {
        var leftOrRight = (Random.Range(0f, 1f) >= .5f) ? XToSpawnFromLeft : XToSpawnFromRight;
        var pos = new Vector3(leftOrRight, (Screen.height - sHeight) / 2);
        return StartMovement(pos, speed);
    }

    public override Vector3 StartMovement(Vector3 startPosition, float speed = 1)
    {
        Speed = speed;
        transform.position = Camera.main.ScreenToWorldPoint(startPosition);

        var ship = GetComponent<EnemyShip>();

        var path = new[]
        {
            // Bottom-middle of the screen
            Camera.main.ScreenToWorldPoint(new Vector3((float)Screen.width/2, sHeight)),
            // Top-middle of the screen
            Camera.main.ScreenToWorldPoint(new Vector3((float)Screen.width/2, Screen.height + sHeight))
        };

        if (ship != null)
        {
            transform.DOPath(path, Speed, PathType.CatmullRom, PathMode.Sidescroller2D)
                .SetSpeedBased(true)
                .SetEase(Ease.Linear)
                .OnComplete(ship.OutOfScreen);
        }

        return startPosition;
    }
}
