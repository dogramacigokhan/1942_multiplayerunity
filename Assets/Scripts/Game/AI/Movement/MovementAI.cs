﻿using UnityEngine;
using System.Collections;

public abstract class MovementAI : MonoBehaviour
{

    public float Speed { get; protected set; }

    protected float sWidth;     // Sprite width
    protected float sHeight;    // Sprite height

    protected TweenPosition TweenPosition;

    protected virtual void OnEnable()
    {
        Init();
    }

    public virtual void Init()
    {
        // Will be used to calculate spawn positions
        var sprite = GetComponent<UISprite>();
        sWidth = (sprite != null) ? sprite.width : 0;
        sHeight = (sprite != null) ? sprite.height : 0;
    }

    public abstract Vector3 StartMovement(float speed = 1);

    public abstract Vector3 StartMovement(Vector3 startPosition, float speed = 1);
}
