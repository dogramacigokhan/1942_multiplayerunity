﻿using DG.Tweening;
using UnityEngine;

public class DirectionalReversedAI : MovementAI
{

    protected float MinXToSpawn;

    protected float MaxXToSpawn;

    void OnDisable()
    {
        transform.DOKill();
    }

    public override void Init()
    {
        base.Init();

        // Spawning must be always inside the screen widths
        MinXToSpawn = sWidth/2;
        MaxXToSpawn = Screen.width - sWidth/2;
    }

    public override Vector3 StartMovement(float speed = 1)
    {
        var pos = new Vector3(Random.Range(MinXToSpawn, MaxXToSpawn), -sHeight);
        return StartMovement(pos, speed);
    }

    public override Vector3 StartMovement(Vector3 startPosition, float speed = 1)
    {
        Speed = speed;
        transform.position = Camera.main.ScreenToWorldPoint(startPosition);

        var endPoint = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height + sHeight)).y;
        var ship = GetComponent<EnemyShip>();

        if (ship != null)
        {
            // Start moving ship from bottom to to of the screen
            transform.DOMoveY(endPoint, Speed).SetSpeedBased(true).SetEase(Ease.Linear).OnComplete(ship.OutOfScreen);
        }

        return startPosition;
    }
}
