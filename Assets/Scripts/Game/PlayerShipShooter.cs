﻿using PathologicalGames;
using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PlayerShipShooter : Photon.MonoBehaviour
{

    public Game Game;                       // Editor
    public int MaxLevel;                    // Editor

    public BulletManager BulletManager;     // Editor

    public float FireRate { get; private set; }
    public int Level;

    private const float InitialFireRate = 0.1f;

    private float _lastFired;

    public void Reset()
    {
        FireRate = InitialFireRate;
        Level = 1;
    }

	void Update () {
	    if ((Input.GetButton("Fire1") || Input.GetKey(KeyCode.Space)) &&
	        Time.timeSinceLevelLoad - _lastFired > FireRate)
        {
            if (Game.GamePaused) 
                return;

            if (PhotonNetwork.offlineMode)
                BulletManager.FireMultiple(transform.position, Level);
            else
                BulletManager.photonView.RPC("FireMultiple", PhotonTargets.All, transform.position, Level);

            _lastFired = Time.timeSinceLevelLoad;
        } 
	}

    [RPC]
    public void Upgrade(PhotonMessageInfo info)
    {
        Level = Mathf.Clamp(Level + 1, 1, MaxLevel);

        if (!PhotonNetwork.offlineMode)
            photonView.RPC("Upgrade", PhotonTargets.Others);
    }
}
