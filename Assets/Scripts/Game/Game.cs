﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using DG.Tweening;
using PathologicalGames;
using UnityEngine;

public class Game : MonoBehaviour, IHandle<GameMessages>, IHandle<EnemyShipDestroyedMessage>
{
    public PauseMenu PauseMenu;             // Editor
    public GameOverMenu GameOverMenu;       // Editor

    public int Level { get; private set; }

    public int Score { get; private set; }

    public int HighScore { get; private set; }

    public Data Data { get; private set; }

    public bool GamePaused { get; private set; }

    private bool _wavesAreComing = true;

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    void Start()
    {
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

    public void TogglePause()
    {
        GamePaused = !GamePaused;

        // Set new time scale and show/hide pause menu
        if (PhotonNetwork.offlineMode)
            Time.timeScale = GamePaused ? 0 : 1;
        
        PauseMenu.gameObject.SetActive(GamePaused);
        EventAggregator.Instance.Publish(GamePaused ? GameMessages.GamePaused : GameMessages.GameResumed);
    }

    public void Init()
    {
        Level = 1;
        Score = 0;

        if (PhotonNetwork.offlineMode)
            GetGameData();  // We need high scores only in singleplayer mode

        gameObject.SetActive(true);
        EventAggregator.Instance.Publish(GameMessages.GameStarted);
    }

    private void GetGameData()
    {
        try
        {
            // Try to get the game data which contains high scores
            Data = GameData.GetData();
            HighScore = Data.HighScores.Max();
        }
        catch (Exception)
        {
            // There is no game data yet, create a new one
            HighScore = 0;
            Data = new Data {HighScores = new List<int>() {0, 0, 0, 0, 0}};
        }

        EventAggregator.Instance.Publish(new HighScoreUpdatedMessage
        {
            NewHighScore = HighScore
        });
    }

    public void Handle(GameMessages message)
    {
        switch (message)
        {
            case GameMessages.WaveCreationStarted:
                _wavesAreComing = true;
                break;
            case GameMessages.WaveCreationStopped:
                _wavesAreComing = false;
                CheckLevelEnd();
                break;
            case GameMessages.EnemyShipIsLost:
                CheckLevelEnd();
                break;
            case GameMessages.GameOverLocally:
                GameOverLocally();
                break;
            case GameMessages.GameOverGlobally:
                GameOverGlobally();
                break;
            case GameMessages.Exit:
                Exit();
                break;
        }
    }

    public void Handle(EnemyShipDestroyedMessage message)
    {
        UpdateScore(message.EnemyShip.ShipType);
        CheckLevelEnd();
    }

    private void UpdateScore(EnemyShipType shipType)
    {
        const int baseScore = 100;

        // Calculate the score based on the level and ship type
        Score += (int) (baseScore*(int) shipType*Level*1.2f);
        EventAggregator.Instance.Publish(new ScoreUpdatedMessage
        {
            NewScore = Score
        });

        if (Score <= HighScore) return;

        // New high score!
        HighScore = Score;
        EventAggregator.Instance.Publish(new HighScoreUpdatedMessage
        {
            NewHighScore = HighScore
        });
    }

    private void CheckLevelEnd()
    {
        if (_wavesAreComing)
            return; // There are still waves to destroy

        if (FindObjectOfType<EnemyShip>() != null)
            return; // Waves are stopped but there are still enemy ships on the screen

        // Level is completed
        EventAggregator.Instance.Publish(new LevelCompletedMessage
        {
            CompletedLevel = Level++
        });
    }

    private void GameOverLocally()
    {
        if (PhotonNetwork.offlineMode)
        {
            PoolManager.Pools["Pool"].DespawnAll();

            // Set new game data with new score
            if (!Data.HighScores.Contains(Score))
            {
                Data.HighScores.Add(Score);
                if (Data.HighScores.Count > 5)
                    Data.HighScores.Remove(Data.HighScores.Min());
            }
            GameData.SetData(Data);
            GameOverMenu.gameObject.SetActive(true);
        }
    }

    private void GameOverGlobally()
    {
        PoolManager.Pools["Pool"].DespawnAll();

        GameOverMenu.gameObject.SetActive(true);
    }

    private void Exit()
    {
        if (GamePaused)
            TogglePause();

        if (!PhotonNetwork.offlineMode)
            PhotonNetwork.Disconnect();

        PoolManager.Pools["Pool"].DespawnAll();

        gameObject.SetActive(false);
    }
} 
