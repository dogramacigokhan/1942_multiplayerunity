﻿using System.Collections;
using DG.Tweening;
using PathologicalGames;
using UnityEngine;

public enum BulletOwnerType
{
    Player,
    Enemy
}

public class Bullet : MonoBehaviour
{

    public BulletOwnerType Owner { get; set; }

    public bool Piercing { get; private set; }

    void OnDisable()
    {
        transform.DOKill();
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var ship = other.GetComponent<Ship>();

        if (ship == null) 
            return;

        var playerShip = other.GetComponent<PlayerShip>();
        if (Owner == BulletOwnerType.Player && playerShip != null)
            return; // Bullet collided with player ship, ignore collision

        var enemyShip = other.GetComponent<EnemyShip>();
        if (Owner == BulletOwnerType.Enemy && enemyShip != null)
            return; // Enemy bullets can not harm each other

        ship.ReceiveDamage(1);

        if (!Piercing)
            Despawn();
    }

    public void Despawn()
    {
        PoolManager.Pools["Pool"].Despawn(transform);
    }
}
