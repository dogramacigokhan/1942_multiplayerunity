﻿using UnityEngine;

public class Background : MonoBehaviour
{

    private const float BackgroundSpeed = .05f;

    private Material _material;

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);

        _material = GetComponent<MeshRenderer>().material;
    }

    void Update()
    {
        // Fill the screen
        var max = Mathf.Max(Screen.width * UIRoot.list[0].activeHeight / Screen.height, UIRoot.list[0].activeHeight);
        transform.localScale = new Vector3(max, max, 1);

        // Move the offsets
        _material.mainTextureOffset = new Vector2(_material.mainTextureOffset.x,
            _material.mainTextureOffset.y + Time.deltaTime*BackgroundSpeed);
    }
}
