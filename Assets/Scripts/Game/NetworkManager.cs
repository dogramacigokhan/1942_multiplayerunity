﻿using UnityEngine;
using System.Collections;

public enum RoomRequest
{
    Create,
    Join
}

public class NetworkManager : Photon.MonoBehaviour, IHandle<OnCreateRoom>, IHandle<OnJoinRoom>, IHandle<OnCancelCreateOrJoinRoom>
{

    public Game Game;           // Editor
    public UILabel Status;      // Editor

    private RoomRequest _roomRequest;
    private string _roomName;

    private bool _connecting;

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    void Update()
    {
        if (!_connecting)
            return;

        Status.text = PhotonNetwork.connectionStateDetailed.ToString();
    }

    public void Handle(OnCreateRoom message)
    {
        _roomRequest = RoomRequest.Create;
        _roomName = message.RoomName;

        Connect(); 
    }

    public void Handle(OnJoinRoom message)
    {
        _roomRequest = RoomRequest.Join;
        _roomName = message.RoomName;

        Connect();
    }

    public void Handle(OnCancelCreateOrJoinRoom message)
    {
        PhotonNetwork.Disconnect();

        Status.gameObject.SetActive(false);
        _connecting = false;
    }

    private void Connect()
    {
        if (PhotonNetwork.offlineMode)
            return;

        _connecting = true;
        Status.gameObject.SetActive(true);

        PhotonNetwork.sendRate = 10;
        PhotonNetwork.sendRate = 10;

        PhotonNetwork.ConnectUsingSettings("1");
    }

    private void Disconnect()
    {
        PhotonNetwork.Disconnect();
    }

    public virtual void OnJoinedLobby()
    {
        if (_roomRequest == RoomRequest.Create)
            PhotonNetwork.CreateRoom(_roomName);
        else
            PhotonNetwork.JoinRoom(_roomName);
    }

    public virtual void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        _connecting = false;
        Status.text = "Disconnected. Cause: " + cause;
    }

    public void OnCreatedRoom()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        EventAggregator.Instance.Publish(GameNetworkMessages.CreatedRoom);

        // We should wait other player to join if we are master (who joins to room first)
        StartCoroutine(WaitForOtherPlayerCoro());
    }

    public void OnPhotonCreateRoomFailed()
    {
        _connecting = false;
        Status.text = "Cannot create room!";
    }

    public void OnJoinedRoom()
    {
        if (PhotonNetwork.isMasterClient)
            return;
        
        EventAggregator.Instance.Publish(GameNetworkMessages.JoinedRoom);
        InitGame();
    }

    public void OnPhotonJoinRoomFailed()
    {
        _connecting = false;
        Status.text = "Cannot join room!";
    }

    public IEnumerator WaitForOtherPlayerCoro()
    {
        while (PhotonNetwork.otherPlayers.Length <= 0)
            yield return null;
        InitGame();
    }

    private void InitGame()
    {
        if (PhotonNetwork.isMasterClient)
            photonView.RPC("EqualizeDimensions", PhotonTargets.Others, Screen.width, Screen.height, Screen.fullScreen);

        Status.gameObject.SetActive(false);
        _connecting = false;
        Game.Init();
    }

    [RPC]
    private void EqualizeDimensions(int width, int height, bool fullScreen, PhotonMessageInfo info)
    {
        Screen.SetResolution(width, height, fullScreen);
    }

    public void OnPhotonPlayerDisconnected(PhotonPlayer player)
    {
        EventAggregator.Instance.Publish(GameNetworkMessages.DisconnectFromServer);

        // We are not master if we joined to the room.
        // We should exit with the master.
        if (_roomRequest == RoomRequest.Join)   
            EventAggregator.Instance.Publish(GameMessages.Exit);
    }
}
