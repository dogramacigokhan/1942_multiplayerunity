﻿using System.Collections;
using DG.Tweening;
using PathologicalGames;
using UnityEngine;

public enum EnemyShipType
{
    SimpleShip = 1,
    SimpleShipV2 = 2,
    SimpleShipV3 = 3
}

public class EnemyShip : Ship
{

    public Transform ExplosionPrefab;       // Editor
    public Transform UpgradePrefab;         // Editor

    private BulletManager _bulletManager;
    private PlayerShip[] _playerShips;

    public EnemyShipType ShipType { get; private set; }

    void OnEnable()
    {
        StartCoroutine(StartShooting());
    }

    void OnDisable()
    {
        if (GetComponent<MovementAI>() != null)
            Destroy(GetComponent<MovementAI>());
    }

    public void Init(EnemyShipType shipType, int hitPoints = 1)
    {
        ShipType = shipType;
        HitPoints = hitPoints;
    }

    public override void ReceiveDamage(int amount)
    {
        HitPoints -= amount;
        if (HitPoints > 0) return;

        Destroy();
    }

    private IEnumerator StartShooting()
    {
        // Set a random shooting interval for each ship
        var shootingInterval = Random.Range(1f, 3f);

        if (!PhotonNetwork.offlineMode && !PhotonNetwork.isMasterClient)
            yield break;    // Only the enemies in the master player's screen can shoot

        _bulletManager = FindObjectOfType<BulletManager>();
        _playerShips = FindObjectsOfType<PlayerShip>();

        while (true)
        {
            yield return new WaitForSeconds(shootingInterval);

            // Select a random player ship and shoot him
            var playerShip = _playerShips[Random.Range(0, _playerShips.Length)];

            if (PhotonNetwork.offlineMode)
                _bulletManager.Fire(transform.position, playerShip.transform.position, (int) BulletOwnerType.Enemy);
            else
                _bulletManager.photonView.RPC("Fire", PhotonTargets.All, 
                    transform.position, playerShip.transform.position, (int) BulletOwnerType.Enemy);
        }
    }

    public void OutOfScreen()
    {
        if (gameObject.activeInHierarchy)
        {
            PoolManager.Pools["Pool"].Despawn(transform);
        }

        EventAggregator.Instance.Publish(GameMessages.EnemyShipIsLost);
    }

    protected override void Destroy(int playerType = 0, PhotonMessageInfo info = null)
    {
        // Destroy animation before despawning enemy ship
        var explosion = PoolManager.Pools["Pool"].Spawn(ExplosionPrefab, transform.parent);
        explosion.localScale = Vector3.one;
        explosion.transform.position = transform.position;

        // Drop a power up bonus if we are lucky enough
        if (PhotonNetwork.offlineMode && Random.Range(0f, 1f) < .05f)
            DropPowerUp();
        
        PoolManager.Pools["Pool"].Despawn(transform);

        EventAggregator.Instance.Publish(new EnemyShipDestroyedMessage
        {
            EnemyShip = this
        });
    }

    private void DropPowerUp()
    {
        var upgrade = PoolManager.Pools["Pool"].Spawn(UpgradePrefab, transform.parent);
        upgrade.localScale = Vector3.one;
        upgrade.transform.position = transform.position;
    }
}
