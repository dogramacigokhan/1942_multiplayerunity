﻿using UnityEngine;
using System.Collections;
using PathologicalGames;

public class Explosion : MonoBehaviour
{

    private Animator _animator;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (!_animator.GetCurrentAnimatorStateInfo(0).IsName("Explosion"))
        {
            PoolManager.Pools["Pool"].Despawn(transform);
        }
    }
}
