﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour, 
    IHandle<GameMessages>, 
    IHandle<LevelCompletedMessage>, 
    IHandle<ScoreUpdatedMessage>, 
    IHandle<HighScoreUpdatedMessage>, 
    IHandle<PlayerShipRevivedMessage>
{

    public Transform Content;           // Editor

    public UISprite Background;         // Editor
    public UILabel LivesLabel;          // Editor
    public UILabel HighScoreLabel;      // Editor
    public UILabel ScoreLabel;          // Editor

    public UILabel LevelLabel;          // Editor
    public float LevelTextDuration;     // Editor

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    public void Handle(GameMessages message)
    {
        switch (message)
        {
            case GameMessages.GameStarted:
                Init();
                break;
            case GameMessages.GameOverLocally:
                if (PhotonNetwork.offlineMode)
                    Content.gameObject.SetActive(false);
                break;
            case GameMessages.GameOverGlobally:
            case GameMessages.Exit:
                Content.gameObject.SetActive(false);
                break;
        }
    }

    public void Handle(ScoreUpdatedMessage message)
    {
        ScoreLabel.text = string.Format(Localization.Get("score"), message.NewScore);
    }

    public void Handle(HighScoreUpdatedMessage message)
    {
        HighScoreLabel.text = string.Format(Localization.Get("highScore"), message.NewHighScore);
    }

    public void Handle(LevelCompletedMessage message)
    {
        LevelLabel.text = string.Format(Localization.Get("levelCompleted"), message.CompletedLevel);
        StartCoroutine(ShowLevelText());
    }

    public void Init()
    {
        LevelLabel.text = string.Format(Localization.Get("level"), 1);
        ScoreLabel.text = string.Format(Localization.Get("score"), 0);
        StartCoroutine(ShowLevelText());

        Content.gameObject.SetActive(true);
    }

    private IEnumerator ShowLevelText()
    {
        LevelLabel.gameObject.SetActive(true);
        yield return new WaitForSeconds(LevelTextDuration);
        LevelLabel.gameObject.SetActive(false);
    }

    public void Handle(PlayerShipRevivedMessage message)
    {
        LivesLabel.text = string.Format(Localization.Get("lives"), message.RemainingLives);
    }
}
