﻿using UnityEngine;

public class MainMenu : MonoBehaviour, IHandle<GameMessages>
{

    public Game Game;                   // Editor
    public GameObject Settings;         // Editor
    public GameObject Multiplayer;      // Editor

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    // Singleplayer button triggers
    public void SinglePlayer()
    {
        PhotonNetwork.offlineMode = true;
        gameObject.SetActive(false);       

        Game.Init();
    }

    // Settings button triggers
    public void ToggleSettings()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
        Settings.SetActive(!Settings.activeInHierarchy);
    }

    // Multiplayer button triggers
    public void ToggleMultiplayerMenu()
    {
        gameObject.SetActive(!gameObject.activeInHierarchy);
        Multiplayer.SetActive(!Multiplayer.activeInHierarchy);
    }

    // Quit button triggers
    public void Quit()
    {
        Application.Quit();
    }

    public void Handle(GameMessages message)
    {
        if (message == GameMessages.Exit)
            gameObject.SetActive(true);
    }
}
