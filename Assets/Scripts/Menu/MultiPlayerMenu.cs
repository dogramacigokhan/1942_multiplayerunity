﻿using System;
using UnityEngine;

public class MultiPlayerMenu : MonoBehaviour, IHandle<GameMessages>
{

    public GameObject Content;              // Editor

    public GameObject Status;               // Editor

    public GameObject BackButton;           // Editor
    public GameObject CancelButton;         // Editor

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    void OnDisable()
    {
        // Be sure all buttons are reverted when the game is started 
        BackButton.SetActive(true);
        CancelButton.SetActive(false);
        Content.SetActive(true);
        Status.SetActive(false);
    }

    // CreateRoom button triggers
    public void CreateRoom(UILabel roomLabel)
    {
        Content.SetActive(false);
        ToggleCancelButton();
        
        PhotonNetwork.offlineMode = false;
        EventAggregator.Instance.Publish(new OnCreateRoom
        {
            RoomName = roomLabel.text.ToLowerInvariant()
        });
    }

    // JoinRoom button triggers
    public void JoinRoom(UILabel roomLabel)
    {
        Content.SetActive(false);
        ToggleCancelButton();

        PhotonNetwork.offlineMode = false;
        EventAggregator.Instance.Publish(new OnJoinRoom
        {
            RoomName = roomLabel.text.ToLowerInvariant()
        });
    }

    // Cancel button triggers
    public void Cancel(UILabel roomLabel)
    {
        EventAggregator.Instance.Publish(new OnCancelCreateOrJoinRoom
        {
            RoomName = roomLabel.text.ToLowerInvariant()
        });

        Content.SetActive(true);
        ToggleCancelButton();
    }

    public void ToggleCancelButton()
    {
        CancelButton.SetActive(!CancelButton.activeInHierarchy);
        BackButton.SetActive(!BackButton.activeInHierarchy);
    }

    public void Handle(GameMessages message)
    {
        if (message == GameMessages.GameStarted)
            gameObject.SetActive(false);       
    }
}
