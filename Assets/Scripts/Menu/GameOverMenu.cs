﻿using System;
using UnityEngine;

public class GameOverMenu : MonoBehaviour, IHandle<GameMessages>
{

    public Game Game;                       // Editor

    public UILabel ScoreLabel;              // Editor
    public UIGrid HighScoresGrid;           // Editor
    public UILabel HighScoresLabel;         // Editor

    public GameObject ReplayButton;         // Editor

    void Awake()
    {
        EventAggregator.Instance.Subscribe(this);
    }

    public void OnEnable()
    {
        ScoreLabel.text = string.Format(Localization.Get("score"), Game.Score);

        ReplayButton.gameObject.SetActive(PhotonNetwork.offlineMode);

        HighScoresGrid.gameObject.SetActive(PhotonNetwork.offlineMode);
        HighScoresLabel.gameObject.SetActive(PhotonNetwork.offlineMode);

        if (!PhotonNetwork.offlineMode)
            return;


        var childs = HighScoresGrid.GetChildList();
        for (var i = 0; i < childs.Count && i < Game.Data.HighScores.Count; i++)
        {
            childs[i].GetComponent<UILabel>().text = Game.Data.HighScores[i].ToString();
        }

        HighScoresGrid.onCustomSort = HighScoresSort;
        HighScoresGrid.Reposition();
    }

    private int HighScoresSort(Transform t1, Transform t2)
    {
        var val1 = int.Parse(t1.GetComponent<UILabel>().text);
        var val2 = int.Parse(t2.GetComponent<UILabel>().text);
        return val1 > val2 ? -1 : val1 < val2 ? 1 : 0;
    }

    // Replay button triggers
    public void ReplayGame()
    {
        Game.Init();
        gameObject.SetActive(false);
    }

    // Exit button triggers
    public void Exit()
    {
        EventAggregator.Instance.Publish(GameMessages.Exit);
        gameObject.SetActive(false);
    }

    public void Handle(GameMessages message)
    {
        if (message == GameMessages.Exit)
            gameObject.SetActive(false);
    }
}
