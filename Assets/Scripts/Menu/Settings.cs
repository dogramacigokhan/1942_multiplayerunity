﻿using System;
using System.Linq;
using UnityEngine;

public enum Language
{
    English,
    Deutsch
}

public class Settings : MonoBehaviour
{
    public Language CurrentLanguage;

    public void ChangeLanguage()
    {
        var languages = Enum.GetValues(typeof (Language)).Cast<Language>().ToList();
        CurrentLanguage = languages[((int)CurrentLanguage + 1)%languages.Count];
        Localization.language = CurrentLanguage.ToString();
    }
}
