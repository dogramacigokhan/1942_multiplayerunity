﻿using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public Game Game;                   // Editor

    // Resume button triggers
    public void ResumeGame()
    {
        Game.TogglePause();
    }

    // Exit button triggers
    public void Exit()
    {
        EventAggregator.Instance.Publish(GameMessages.Exit);
    }
}
